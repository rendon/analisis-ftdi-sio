#include <linux/kernel.h>
#include <linux/errno.h>
#include <linux/init.h>
#include <linux/slab.h>
#include <linux/tty.h>
#include <linux/tty_driver.h>
#include <linux/tty_flip.h>
#include <linux/module.h>
#include <linux/spinlock.h>
#include <linux/mutex.h>
#include <linux/uaccess.h>
#include <linux/usb.h>
#include <linux/serial.h>
#include <linux/usb/serial.h>
#include "ftdi_sio.h"

#define DRIVER_AUTHOR "Greg, Bill, Kuba, Andreas, Johan"
#define DRIVER_DESC "USB FTDI Serial Converters Driver"

#define FTDI_VID	0x0403	/* Vendor Id */
/*** "original" FTDI device PIDs ***/
#define FTDI_8U232AM_PID 0x6001 /* Similar device to SIO above */

struct ftdi_private {
	struct kref kref;
	enum ftdi_chip_type chip_type;
				        /* type of device, either SIO or FT8U232AM */
	int baud_base;		/* baud base clock for divisor setting */
	int custom_divisor;	/* custom_divisor kludge, this is for
				        baud_base (different from what goes to the chip!) */
	__u16 last_set_data_urb_value ;
				/* the last data state set - needed for doing
				 * a break
				 */
	int flags;		/* some ASYNC_xxxx flags are supported */
	unsigned long last_dtr_rts;	/* saved modem control outputs */
	struct async_icount	icount;
	wait_queue_head_t delta_msr_wait; /* Used for TIOCMIWAIT */
	char prev_status;        /* Used for TIOCMIWAIT */
	bool dev_gone;        /* Used to abort TIOCMIWAIT */
	char transmit_empty;	/* If transmitter is empty or not */
	__u16 interface;	/* FT2232C, FT2232H or FT4232H port interface
				   (0 for FT232/245) */

	speed_t force_baud;	/* if non-zero, force the baud rate to
				   this value */
	int force_rtscts;	/* if non-zero, force RTS-CTS to always
				   be enabled */

	unsigned int latency;		/* latency setting in use */
	unsigned short max_packet_size;
	struct mutex cfg_lock; /* Avoid mess by parallel calls of config ioctl() and change_speed() */
};

/*
 * Device ID not listed? Test via module params product/vendor or
 * /sys/bus/usb/ftdi_sio/new_id, then send patch/report!
 */
static struct usb_device_id id_table_combined [] = {
	{ USB_DEVICE(FTDI_VID, FTDI_8U232AM_PID) }, /* Este es el bueno.*/
	{ }					/* Terminating entry */
};

MODULE_DEVICE_TABLE(usb, id_table_combined);

static const char *ftdi_chip_name[] = {
	[FT232RL] = "FT232RL"
};


/* Used for TIOCMIWAIT */
#define FTDI_STATUS_B0_MASK	(FTDI_RS0_CTS | FTDI_RS0_DSR | FTDI_RS0_RI | FTDI_RS0_RLSD)
/* End TIOCMIWAIT */

/* function prototypes for a FTDI serial converter */
static int  ftdi_sio_probe(struct usb_serial *serial,
					const struct usb_device_id *id);
static int  ftdi_sio_port_probe(struct usb_serial_port *port);
static int  ftdi_sio_port_remove(struct usb_serial_port *port);
static int  ftdi_open(struct tty_struct *tty, struct usb_serial_port *port);
static void ftdi_close(struct usb_serial_port *port);
static void ftdi_process_read_urb(struct urb *urb);
static int ftdi_prepare_write_buffer(struct usb_serial_port *port,
						             void *dest, size_t size);
static void ftdi_set_termios(struct tty_struct *tty,
			struct usb_serial_port *port, struct ktermios *old);

static __u32 ftdi_232bm_baud_base_to_divisor(int baud, int base);
static __u32 ftdi_232bm_baud_to_divisor(int baud);

static struct usb_serial_driver ftdi_sio_device = {
	.driver = {
		.owner =	THIS_MODULE,
		.name =		"ftdi_sio",
	},
	.description            =	"FTDI USB Serial Device",
	.id_table               =	id_table_combined,
	.num_ports              =	1,
	.bulk_in_size           =	512,
	.bulk_out_size          =   256,
	.probe                  =	ftdi_sio_probe,
	.port_probe             =	ftdi_sio_port_probe,
	.port_remove            =	ftdi_sio_port_remove,
	.open                   =   ftdi_open,
	.close                  =	ftdi_close,
	.throttle               =	usb_serial_generic_throttle,
	.unthrottle             =	usb_serial_generic_unthrottle,
	.process_read_urb       =	ftdi_process_read_urb,
	.prepare_write_buffer   =	ftdi_prepare_write_buffer,
	.set_termios            =   ftdi_set_termios,
};

static struct usb_serial_driver * const serial_drivers[] = {
	&ftdi_sio_device, NULL
};


#define WDR_TIMEOUT 5000 /* default urb timeout */
#define WDR_SHORT_TIMEOUT 1000	/* shorter urb timeout */

/* High and low are for DTR, RTS etc etc */
#define HIGH 1
#define LOW 0

/* ***************************************************************************
* Utility functions
* ***************************************************************************/
static __u32 ftdi_232bm_baud_base_to_divisor(int baud, int base)
{
	static const unsigned char divfrac[8] = { 0, 3, 2, 4, 1, 5, 6, 7 };
	__u32 divisor;
	/* divisor shifted 3 bits to the left */
	int divisor3 = base / 2 / baud;
	divisor = divisor3 >> 3;
	divisor |= (__u32)divfrac[divisor3 & 0x7] << 14;
	/* Deal with special cases for highest baud rates. */
	if (divisor == 1)
		divisor = 0;
	else if (divisor == 0x4001)
		divisor = 1;
	return divisor;
}

static __u32 ftdi_232bm_baud_to_divisor(int baud)
{
	 return ftdi_232bm_baud_base_to_divisor(baud, 48000000);
}


#define set_mctrl(port, set)		update_mctrl((port), (set), 0)
#define clear_mctrl(port, clear)	update_mctrl((port), 0, (clear))

static int update_mctrl(struct usb_serial_port *port, unsigned int set,
							unsigned int clear)
{
	struct ftdi_private *priv = usb_get_serial_port_data(port);
	struct device *dev = &port->dev;
	unsigned urb_value;
	int rv;

	if (((set | clear) & (TIOCM_DTR | TIOCM_RTS)) == 0) {
		dev_dbg(dev, "%s - DTR|RTS not being set|cleared\n", __func__);
		return 0;	/* no change */
	}

	clear &= ~set;	/* 'set' takes precedence over 'clear' */
	urb_value = 0;
	if (clear & TIOCM_DTR)
		urb_value |= FTDI_SIO_SET_DTR_LOW;
	if (clear & TIOCM_RTS)
		urb_value |= FTDI_SIO_SET_RTS_LOW;
	if (set & TIOCM_DTR)
		urb_value |= FTDI_SIO_SET_DTR_HIGH;
	if (set & TIOCM_RTS)
		urb_value |= FTDI_SIO_SET_RTS_HIGH;
	rv = usb_control_msg(port->serial->dev,
			       usb_sndctrlpipe(port->serial->dev, 0),
			       FTDI_SIO_SET_MODEM_CTRL_REQUEST,
			       FTDI_SIO_SET_MODEM_CTRL_REQUEST_TYPE,
			       urb_value, priv->interface,
			       NULL, 0, WDR_TIMEOUT);
	if (rv < 0) {
		dev_dbg(dev, "%s Error from MODEM_CTRL urb: DTR %s, RTS %s\n",
			__func__,
			(set & TIOCM_DTR) ? "HIGH" : (clear & TIOCM_DTR) ? "LOW" : "unchanged",
			(set & TIOCM_RTS) ? "HIGH" : (clear & TIOCM_RTS) ? "LOW" : "unchanged");
		rv = usb_translate_errors(rv);
	} else {
		dev_dbg(dev, "%s - DTR %s, RTS %s\n", __func__,
			(set & TIOCM_DTR) ? "HIGH" : (clear & TIOCM_DTR) ? "LOW" : "unchanged",
			(set & TIOCM_RTS) ? "HIGH" : (clear & TIOCM_RTS) ? "LOW" : "unchanged");
		/* FIXME: locking on last_dtr_rts */
		priv->last_dtr_rts = (priv->last_dtr_rts & ~clear) | set;
	}
	return rv;
}


static __u32 get_ftdi_divisor(struct tty_struct *tty,
						struct usb_serial_port *port)
{
	struct ftdi_private *priv = usb_get_serial_port_data(port);
	struct device *dev = &port->dev;
	__u32 div_value = 0;
	int div_okay = 1;
	int baud;

	/*
	 * The logic involved in setting the baudrate can be cleanly split into
	 * 3 steps.
	 * 1. Standard baud rates are set in tty->termios->c_cflag
	 * 2. If these are not enough, you can set any speed using alt_speed as
	 * follows:
	 *    - set tty->termios->c_cflag speed to B38400
	 *    - set your real speed in tty->alt_speed; it gets ignored when
	 *      alt_speed==0, (or)
	 *    - call TIOCSSERIAL ioctl with (struct serial_struct) set as
	 *	follows:
	 *      flags & ASYNC_SPD_MASK == ASYNC_SPD_[HI, VHI, SHI, WARP],
	 *	this just sets alt_speed to (HI: 57600, VHI: 115200,
	 *	SHI: 230400, WARP: 460800)
	 * ** Steps 1, 2 are done courtesy of tty_get_baud_rate
	 * 3. You can also set baud rate by setting custom divisor as follows
	 *    - set tty->termios->c_cflag speed to B38400
	 *    - call TIOCSSERIAL ioctl with (struct serial_struct) set as
	 *	follows:
	 *      o flags & ASYNC_SPD_MASK == ASYNC_SPD_CUST
	 *      o custom_divisor set to baud_base / your_new_baudrate
	 * ** Step 3 is done courtesy of code borrowed from serial.c
	 *    I should really spend some time and separate + move this common
	 *    code to serial.c, it is replicated in nearly every serial driver
	 *    you see.
	 */

	/* 1. Get the baud rate from the tty settings, this observes
	      alt_speed hack */

	baud = tty_get_baud_rate(tty);
	dev_dbg(dev, "%s - tty_get_baud_rate reports speed %d\n", __func__, baud);

	/* 2. Observe async-compatible custom_divisor hack, update baudrate
	   if needed */

	if (baud == 38400 &&
	    ((priv->flags & ASYNC_SPD_MASK) == ASYNC_SPD_CUST) &&
	     (priv->custom_divisor)) {
		baud = priv->baud_base / priv->custom_divisor;
		dev_dbg(dev, "%s - custom divisor %d sets baud rate to %d\n",
			__func__, priv->custom_divisor, baud);
	}

	/* 3. Convert baudrate to device-specific divisor */

	if (!baud)
		baud = 9600;
	switch (priv->chip_type) {
	case FT232RL: /* FT232RL chip */
		if (baud <= 3000000) {
			div_value = ftdi_232bm_baud_to_divisor(baud);
		}
		break;
	} /* priv->chip_type */

	if (div_okay) {
		dev_dbg(dev, "%s - Baud rate set to %d (divisor 0x%lX) on chip %s\n",
			__func__, baud, (unsigned long)div_value,
			ftdi_chip_name[priv->chip_type]);
	}

	tty_encode_baud_rate(tty, baud, baud);
	return div_value;
}

static int change_speed(struct tty_struct *tty, struct usb_serial_port *port)
{
	//struct ftdi_private *priv = usb_get_serial_port_data(port);
	__u16 urb_value;
	__u16 urb_index;
	__u32 urb_index_value;
	int rv;

	urb_index_value = get_ftdi_divisor(tty, port);
	urb_value = (__u16)urb_index_value;
	urb_index = (__u16)(urb_index_value >> 16);

	rv = usb_control_msg(port->serial->dev,
			    usb_sndctrlpipe(port->serial->dev, 0),
			    FTDI_SIO_SET_BAUDRATE_REQUEST,
			    FTDI_SIO_SET_BAUDRATE_REQUEST_TYPE,
			    urb_value, urb_index,
			    NULL, 0, WDR_SHORT_TIMEOUT);
	return rv;
}

static int write_latency_timer(struct usb_serial_port *port)
{
	struct ftdi_private *priv = usb_get_serial_port_data(port);
	struct usb_device *udev = port->serial->dev;
	int rv;
	int l = priv->latency;

	if (priv->flags & ASYNC_LOW_LATENCY)
		l = 1;

	dev_dbg(&port->dev, "%s: setting latency timer = %i\n", __func__, l);
	rv = usb_control_msg(udev,
			     usb_sndctrlpipe(udev, 0),
			     FTDI_SIO_SET_LATENCY_TIMER_REQUEST,
			     FTDI_SIO_SET_LATENCY_TIMER_REQUEST_TYPE,
			     l, priv->interface,
			     NULL, 0, WDR_TIMEOUT);
	if (rv < 0)
		dev_err(&port->dev, "Unable to write latency timer: %i\n", rv);
	return rv;
}

/*
 * ***************************************************************************
 * FTDI driver specific functions
 * ***************************************************************************
 */

/* Probe function to check for special devices */
static int ftdi_sio_probe(struct usb_serial *serial,
					const struct usb_device_id *id)
{
	usb_set_serial_data(serial, (void *)id->driver_info);
	return 0;
}

static int ftdi_sio_port_probe(struct usb_serial_port *port)
{
	struct ftdi_private *priv;

	priv = kzalloc(sizeof(struct ftdi_private), GFP_KERNEL);
	if (!priv) {
		dev_err(&port->dev, "%s- kmalloc(%Zd) failed.\n", __func__,
					sizeof(struct ftdi_private));
		return -ENOMEM;
	}

	kref_init(&priv->kref);
	mutex_init(&priv->cfg_lock);
	init_waitqueue_head(&priv->delta_msr_wait);

	priv->flags = ASYNC_LOW_LATENCY;
	priv->dev_gone = false;

	usb_set_serial_port_data(port, priv);
    priv->chip_type = FT232RL;
    priv->max_packet_size = 64;

    priv->latency = 1; // By inspection
	write_latency_timer(port);
	return 0;
}

/*
 * Module parameter to control latency timer for NDI FTDI-based USB devices.
 * If this value is not set in /etc/modprobe.d/ its value will be set
 * to 1ms.
 */
static void ftdi_sio_priv_release(struct kref *k)
{
	struct ftdi_private *priv = container_of(k, struct ftdi_private, kref);

	kfree(priv);
}

static int ftdi_sio_port_remove(struct usb_serial_port *port)
{
	struct ftdi_private *priv = usb_get_serial_port_data(port);

	priv->dev_gone = true;
	wake_up_interruptible_all(&priv->delta_msr_wait);
	kref_put(&priv->kref, ftdi_sio_priv_release);

	return 0;
}

static int ftdi_open(struct tty_struct *tty, struct usb_serial_port *port)
{
	struct ktermios dummy;
	struct usb_device *dev = port->serial->dev;
	struct ftdi_private *priv = usb_get_serial_port_data(port);
	int result;

	/* No error checking for this (will get errors later anyway) */
	/* See ftdi_sio.h for description of what is reset */
	usb_control_msg(dev, usb_sndctrlpipe(dev, 0),
			FTDI_SIO_RESET_REQUEST, FTDI_SIO_RESET_REQUEST_TYPE,
			FTDI_SIO_RESET_SIO,
			priv->interface, NULL, 0, WDR_TIMEOUT);

	/* Termios defaults are set by usb_serial_init. We don't change
	   port->tty->termios - this would lose speed settings, etc.
	   This is same behaviour as serial.c/rs_open() - Kuba */

	/* ftdi_set_termios  will send usb control messages */
	if (tty) {
		memset(&dummy, 0, sizeof(dummy));
		ftdi_set_termios(tty, port, &dummy);
	}

	/* Start reading from the device */
	result = usb_serial_generic_open(tty, port);
	if (!result)
		kref_get(&priv->kref);

	return result;
}

/*
 * usbserial:__serial_close  only calls ftdi_close if the point is open
 *   This only gets called when it is the last close
 */
static void ftdi_close(struct usb_serial_port *port)
{
	struct ftdi_private *priv = usb_get_serial_port_data(port);

	usb_serial_generic_close(port);
	kref_put(&priv->kref, ftdi_sio_priv_release);
}

/* The SIO requires the first byte to have:
 *  B0 1
 *  B1 0
 *  B2..7 length of message excluding byte 0
 *
 * The new devices do not require this byte
 */
static int ftdi_prepare_write_buffer(struct usb_serial_port *port,
						void *dest, size_t size)
{
	struct ftdi_private *priv;
	int count;
	priv = usb_get_serial_port_data(port);
    count = kfifo_out_locked(&port->write_fifo, dest, size, &port->lock);
    priv->icount.tx += count;
	return count;
}

#define FTDI_RS_ERR_MASK (FTDI_RS_BI | FTDI_RS_PE | FTDI_RS_FE | FTDI_RS_OE)

static int ftdi_process_packet(struct tty_struct *tty,
		struct usb_serial_port *port, struct ftdi_private *priv,
		char *packet, int len)
{
	int i;
	char status;
	char flag;
	char *ch;

	if (len < 2) {
		dev_dbg(&port->dev, "malformed packet\n");
		return 0;
	}

	/* Compare new line status to the old one, signal if different/
	   N.B. packet may be processed more than once, but differences
	   are only processed once.  */
	status = packet[0] & FTDI_STATUS_B0_MASK;
	if (status != priv->prev_status) {
		char diff_status = status ^ priv->prev_status;

		if (diff_status & FTDI_RS0_CTS)
			priv->icount.cts++;
		if (diff_status & FTDI_RS0_DSR)
			priv->icount.dsr++;
		if (diff_status & FTDI_RS0_RI)
			priv->icount.rng++;
		if (diff_status & FTDI_RS0_RLSD)
			priv->icount.dcd++;

		wake_up_interruptible_all(&priv->delta_msr_wait);
		priv->prev_status = status;
	}

	flag = TTY_NORMAL;
	if (packet[1] & FTDI_RS_ERR_MASK) {
		/* Break takes precedence over parity, which takes precedence
		 * over framing errors */
		if (packet[1] & FTDI_RS_BI) {
			flag = TTY_BREAK;
			priv->icount.brk++;
			usb_serial_handle_break(port);
		} else if (packet[1] & FTDI_RS_PE) {
			flag = TTY_PARITY;
			priv->icount.parity++;
		} else if (packet[1] & FTDI_RS_FE) {
			flag = TTY_FRAME;
			priv->icount.frame++;
		}
		/* Overrun is special, not associated with a char */
		if (packet[1] & FTDI_RS_OE) {
			priv->icount.overrun++;
			tty_insert_flip_char(tty, 0, TTY_OVERRUN);
		}
	}

	/* save if the transmitter is empty or not */
	if (packet[1] & FTDI_RS_TEMT)
		priv->transmit_empty = 1;
	else
		priv->transmit_empty = 0;

	len -= 2;
	if (!len)
		return 0;	/* status only */
	priv->icount.rx += len;
	ch = packet + 2;

	if (port->port.console && port->sysrq) {
		for (i = 0; i < len; i++, ch++) {
			if (!usb_serial_handle_sysrq_char(port, *ch))
				tty_insert_flip_char(tty, *ch, flag);
		}
	} else {
		tty_insert_flip_string_fixed_flag(tty, ch, flag, len);
	}

	return len;
}

static void ftdi_process_read_urb(struct urb *urb)
{
	struct usb_serial_port *port = urb->context;
	struct tty_struct *tty;
	struct ftdi_private *priv = usb_get_serial_port_data(port);
	char *data = (char *)urb->transfer_buffer;
	int i;
	int len;
	int count = 0;

	tty = tty_port_tty_get(&port->port);
	if (!tty)
		return;

	for (i = 0; i < urb->actual_length; i += priv->max_packet_size) {
		len = min_t(int, urb->actual_length - i, priv->max_packet_size);
		count += ftdi_process_packet(tty, port, priv, &data[i], len);
	}

	if (count)
		tty_flip_buffer_push(tty);
	tty_kref_put(tty);
}

/* old_termios contains the original termios settings and tty->termios contains
 * the new setting to be used
 * WARNING: set_termios calls this with old_termios in kernel space
 */
static void ftdi_set_termios(struct tty_struct *tty,
		struct usb_serial_port *port, struct ktermios *old_termios)
{
	struct usb_device *dev = port->serial->dev;
	struct ftdi_private *priv = usb_get_serial_port_data(port);
	struct ktermios *termios = &tty->termios;
	unsigned int cflag = termios->c_cflag;
	__u16 urb_value; /* will hold the new flags */

	/* Added for xon/xoff support */
    int step = 1;
    printk(KERN_INFO "__ STEP %d __\n", step++);

	cflag = termios->c_cflag;

	if (!old_termios)
		goto no_skip;

	/* NOTE These routines can get interrupted by
	   ftdi_sio_read_bulk_callback  - need to examine what this means -
	   don't see any problems yet */

	if ((old_termios->c_cflag & (CSIZE|PARODD|PARENB|CMSPAR|CSTOPB)) ==
	    (termios->c_cflag & (CSIZE|PARODD|PARENB|CMSPAR|CSTOPB)))
		goto no_data_parity_stop_changes;

no_skip:
	/* Set number of data bits, parity, stop bits */
	urb_value = 0;
	urb_value |= (cflag & CSTOPB ? FTDI_SIO_SET_DATA_STOP_BITS_2 :
		      FTDI_SIO_SET_DATA_STOP_BITS_1);

    urb_value |= FTDI_SIO_SET_DATA_PARITY_NONE;
    urb_value |= 8; // Caracter size C8

	/* This is needed by the break command since it uses the same command
	   - but is or'ed with this value  */
	priv->last_set_data_urb_value = urb_value;

	if (usb_control_msg(dev, usb_sndctrlpipe(dev, 0),
			    FTDI_SIO_SET_DATA_REQUEST,
			    FTDI_SIO_SET_DATA_REQUEST_TYPE,
			    urb_value , priv->interface,
			    NULL, 0, WDR_SHORT_TIMEOUT) < 0) {
		printk(KERN_INFO "%s FAILED to set databits/stopbits/parity\n",
			__func__);
	}

	/* Now do the baudrate */
no_data_parity_stop_changes:
    /* set the baudrate determined before */
    mutex_lock(&priv->cfg_lock);
    if (change_speed(tty, port))
        printk(KERN_INFO "%s urb failed to set baudrate\n", __func__);
    mutex_unlock(&priv->cfg_lock);
    /* Ensure RTS and DTR are raised when baudrate changed from 0 */
    if (!old_termios || (old_termios->c_cflag & CBAUD) == B0) {
        set_mctrl(port, TIOCM_DTR | TIOCM_RTS);
    }
}

static int __init ftdi_init(void)
{
	return usb_serial_register_drivers(serial_drivers, KBUILD_MODNAME, id_table_combined);
}

static void __exit ftdi_exit(void)
{
	usb_serial_deregister_drivers(serial_drivers);
}

module_init(ftdi_init);
module_exit(ftdi_exit);

MODULE_AUTHOR(DRIVER_AUTHOR);
MODULE_DESCRIPTION(DRIVER_DESC);
MODULE_LICENSE("GPL");
