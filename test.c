// Compile with: g++ -Wall -o snake snake.cpp -lcurses
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <unistd.h>
#include <fcntl.h>
#include <termios.h>
 
const int DELAY = 500 * 1000; // microseconds
 
int main(int argc, char** argv)
{
  struct termios tio;
  int tty_fd;
  char ch;
  char *device = "/dev/ttyUSB0"; // Your serial device
 
  memset(&tio,0,sizeof(tio));
  tio.c_iflag = 0;
  tio.c_oflag = 0;
  tio.c_cflag = CS8 | CREAD | CLOCAL;
  tio.c_lflag = 0;
  tio.c_cc[VMIN] = 1;
  tio.c_cc[VTIME] = 5;
 
 
  tty_fd = open(device, O_RDWR | O_NONBLOCK);      
  cfsetospeed(&tio,B9600);
  cfsetispeed(&tio,B9600);
 
  tcsetattr(tty_fd,TCSANOW,&tio);
 
  while (1) {
      while (read(tty_fd, &ch, 1) > 0)
          putchar(ch);
  }

  usleep(100000);
  close(tty_fd);
 
  return EXIT_SUCCESS;
}
