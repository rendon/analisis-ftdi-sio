#
# Makefile for the USB serial device drivers.
#

# Object file lists.

obj-m += ftdi_sio.o

all: clean
	make -C /lib/modules/$(shell uname -r)/build M=$(PWD) modules
clean:
	make -C /lib/modules/$(shell uname -r)/build M=$(PWD) clean


